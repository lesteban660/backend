'use strict'

const mongoose = require ('mongoose');
const app = require ('../Server/index');
const port = 3900;

//Generar promesa global
mongoose.Promise = global.Promise

//Hacer conexion a mongo

mongoose.connect('mongodb://localhost:27017/futbol', { useNewUrlParser : true })
    .then(( ) => {
        console.log('Base de datos corriendo en local');

        //Escuchar servidor
        app.listen(port, () => {
            console.log(`Server corriendo en el puerto: ${port}`)
        })
    })