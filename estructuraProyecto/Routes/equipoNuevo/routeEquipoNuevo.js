const express = require('express');
const modelEquipo = require('../../Models/Equipos/modelEquipo');

let app = express();

app.post('/equipo/nuevo', (req, res) =>{
    let body = req.body;
    console.log(body);

    let newSchemaEquipo = new modelEquipo({
        nombre: body.nombre,
        liga: body.liga,
        dt: body.dt,
        noIntegrantes: body.noIntegrantes,
        categoria: body.categoria
    });

    newSchemaEquipo
        .save()
        .then(
            (data)=>{
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Guardado',
                        data
                    });
            }
        )
        .catch(
            (err) =>{
                return res.status(500)
                .json({
                    ok : false,
                    message: 'Ocurrio un error',
                    err
                });
            }
        )
});


app.get('/todos/equipos', async(req, res) =>{
    await modelEquipo
        .find()
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Encontrados con éxito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

app.get('/id/equipo/:id', async(req, res) =>{
    await modelEquipo
        .findById(req.params.id)
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Encontrados con éxito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})


app.put('/actualizar/equipo/:id', async(req, res) =>{
    const{nombre,liga,dt,noIntegrantes,categoria}=req.body;

    let equipo= await modelEquipo
        .findById(req.params.id)

        equipo.nombre = nombre,
        equipo.liga = liga,
        equipo.dt = dt,
        equipo.noIntegrantes = noIntegrantes,
        equipo.categoria = categoria

        equipo=await modelEquipo.findOneAndUpdate({_id: req.params.id},equipo,{new:true})

        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Se han actualizado los datos',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})


app.delete('/borrar/equipo/:id', async(req, res) =>{
    let equipo= await modelEquipo
        .findById(req.params.id)

        await modelEquipo.findOneAndRemove({_id: req.params.id})
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Ya se han borrado los datos.',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Vayaaaa... ha ocurrido un error',
                        err
                    });
            }
        );
})

module.exports = app;