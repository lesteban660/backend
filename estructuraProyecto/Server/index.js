'use strict'
//Lamar las librerias
const bodyParser = require ('body-parser');
const express = require ('express');
 
//iniciamos express

const app = express();

//activar cors
app.use(function (req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Header", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

//MiddleWares de express
app.use(bodyParser.urlencoded({extended: false }));//no acepta cualquier documeto, lo convierte a json
app.use(bodyParser.json());

app.use(require('../Routes/index'));//pasar ruta de los controladores

module.exports = app;