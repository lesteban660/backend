const mongose = require("mongoose");
let Schema = mongose.Schema;

let nuevoEquipo =  new Schema({
    nombre: { type: String },
    liga: { type: String },
    dt: { type: String },
    noIntegrantes: { type: Number },
    categoria: { type: String }
});

module.exports = mongose.model('equipo', nuevoEquipo);