const mongose = require("mongoose");
let Schema = mongose.Schema;

let nuevoFutbolista =  new Schema({
    nombre: { type: String },
    apellidoPaterno: { type: String },
    apellidoMaterno: { type: String },
    nacionalidad: { type: String },
    posicion: { type: String },
    edad: { type: Number },
    altura: { type: Number },
    equipo: { type: String },
    peso: { type: String },
    clubProcedencia: { type: String },
    perfilPateo: { type: String },
});

module.exports = mongose.model('futbolista', nuevoFutbolista);